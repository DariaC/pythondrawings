from turtle import*
from random import*

lt(90)

lv = 10 # broj
l = 120 # dužina
s = 35 # kut

color("#5E2612")

def draw_tree(l, level, lv):
    
    if level == 1:
        width(lv)
        penup()
        bk(l)
        pendown()
        fd(l)
        
    width_e = width()  # save the current pen width

    width(width_e * 0.75)  # sužavanje debljine
    
    if(level == (lv-1)):
        color("limegreen")

    l = 0.75 * l # smanjenje dužine sa svakom granom

    lt(s)
    fd(l)

    if level < lv:
        draw_tree(l, level + 1, lv)
        color("#5E2612")

    bk(l)
    rt(2 * s)
    fd(l)

    if(level == (lv-1)):
        color("limegreen")
        
    if level < lv:
        draw_tree(l, level + 1, lv)
        color("#5E2612")
        
    bk(l)
    lt(s)

    width(width_e)  # restore the previous pen width

speed("fastest")

draw_tree(l, 1, 11)


exitonclick()