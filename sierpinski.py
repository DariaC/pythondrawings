from turtle import*

def sierpinski(duzina ,dubina):
    
    if dubina == 0:
        begin_fill() 
        for i in range(0,3): 
            fd(duzina) 
            lt(120)
        end_fill() 
        
    else:
        sierpinski(duzina/2, dubina - 1)
        fd(duzina/2)
        sierpinski(duzina/2, dubina-1) 
        bk(duzina/2) 
        lt(60) 
        fd(duzina/2) 
        rt(60)
        sierpinski(duzina/2, dubina-1)
        lt(60)
        bk(duzina/2)
        rt(60)

speed(0) 
bgcolor("black") 
color("white")         
pu() 
goto(-250, -200) 
pd() 
sierpinski(500, 5) 
exitonclick() 

