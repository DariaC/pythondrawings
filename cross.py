from turtle import*


speed("fastest")
color('blue')

def draw_cross(x, y, length):
    up()
    goto(x - length/2, y - length/6)
    down()
    seth(0)
    begin_fill()
    for i in range(4):
        fd(length/3)
        right(90)
        fd(length/3)
        left(90)
        fd(length/3)
        left(90)
        
    end_fill()

def vicsek(x, y, length, n):
    if n == 0:
        draw_cross(x, y, length)
        return

    vicsek(x, y, length/3, n - 1)
    vicsek(x + length/3, y, length/3, n - 1)
    vicsek(x - length/3, y, length/3, n - 1)
    vicsek(x, y + length/3, length/3, n - 1)
    vicsek(x, y - length/3, length/3, n - 1)
    
vicsek(0, 0, 400, 4)

exitonclick()