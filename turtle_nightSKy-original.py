from turtle import*
from random import*
  
speed(0)
bgcolor("black")
color("white")
title("Noćno nebo")
  
def stars(n):
    for i in range(5):
        fd(n)
        rt(144)


for i in range(60):
    x1 = randrange(-500, 550)
    y1 = randrange(-300, 300)
    x2 = randrange(-500, 550)
    y2 = randrange(-300, 300)
    x3 = randrange(-500, 550)
    y3 = randrange(-300, 300)
    pu()
    setpos(x1, y1)
    pd()
    stars(6)
    pu()
    setpos(x2, y2)
    pd()
    stars(8)
    pu()
    setpos(x3, y3)
    pd()
    begin_fill()
    circle(1)
    end_fill()

pu()
setpos(0, 0)
pd()
color("white")
begin_fill()
circle(70)
end_fill()
pu()
setpos(30, 0)
pd()
color("black")
begin_fill()
circle(70)
end_fill()
ht()
exitonclick()